import {createRouter, createWebHistory} from 'vue-router'
// import Category from '../views/category/Category'
// import Home from '../views/home/Home'
// import Profile from '../views/profile/Profile'
// import Shopcart from '../views/shopcart/Shopcart'

const Home = () => import('../views/home/Home')
const Category = () => import('../views/category/Category')
const Shopcart = () => import('../views/shopcart/Shopcart')
const Profile = () => import('../views/profile/Profile')



// 2、创建路由对象
const routes = [
    {
        path: '',
        // redirect重定向
        redirect: '/home'
      },
      {
        path: '/home',
        component: Home
      },
      {
        path: '/profile',
        component: Profile
      },
      {
        path: '/category',
        component: Category
      },
      {
        path: '/shopcart',
        component: Shopcart
      }
]
const router = createRouter({
    history: createWebHistory(),
    routes
})

//3、导出router
export default router

